%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%  Rafi Pelossof, MSKCC, 2014
%

function [model] = ar_train(A,B,Y,lambda, rsL2, spectrumA, spectrumB)
% train the affinity regression
% A is the left matrix (D), B is the right matrix transposed (P')

% solution through lower dimensional SVD(A), SVD(B)
[UA SA VA] = svd(A);
[UB SB VB] = svd(B);
%da = min(size(SA));

a_spectrum = diag(SA);
b_spectrum = diag(SB);
a_cum_spectrum = cumsum(a_spectrum)/sum(a_spectrum);
b_cum_spectrum = cumsum(b_spectrum)/sum(b_spectrum);

da = find(a_cum_spectrum >= spectrumA, 1 , 'first');
db = find(b_cum_spectrum >= spectrumB, 1 , 'first');

Ua = UA(:,1:da); Sa = SA(1:da,1:da); Va = VA(:,1:da);
Ub = UB(:,1:db); Sb = SB(1:db,1:db); Vb = VB(:,1:db);

Yv = Y(:);
L = kron(Vb,Ua);

% remove elements from the diagonal
if 1
    d = eye(size(Y));
    Yv(find(d)) = [];
    L(find(d),:) = [];
end

opts.rsL2=rsL2;

fprintf('ar_train.m: running LeastR L=(%d x %d), Yv=%d, lambda=%.3f, rsL2=%.3f, specA=%.3f, specB=%.3f\n', size(L,1), size(L,2), length(Yv), lambda, opts.rsL2, spectrumA, spectrumB);
start = tic();
[beta b] = LeastR(L, Yv, lambda, opts);
end_time = toc(start);

model.Ua = Ua;
model.Ub = Ub;
model.Sa = Sa;
model.Sb = Sb;
model.Va = Va;
model.Vb = Vb;
model.fit_svd.beta = beta;
model.fit_svd.lambda = lambda;
model.fit_svd.pred = b;
model.end_time = end_time;
