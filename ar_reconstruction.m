%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%  Rafi Pelossof, MSKCC, 2014
%


function pred = reconstruction(y_train, pred_test)
A = y_train' * pred_test;

O = orth(y_train);
c = O\y_train;
ct = c'\A; %equivalent to: inv(c')*A;

pred = O*ct;

