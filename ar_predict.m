%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%  Rafi Pelossof, MSKCC, 2014
%

function predictions= ar_predict(D, P_test, Y_train, model, n_nn)
% loop over all lambdas, reconstruct models (w) and return predictions
% in the case where the output space is highly non-linear, reconstruction
% may work poorly. n_nn would return the weighted average of the n nearest
% neighbors to the prediction.

n_probes = size(Y_train, 1);
n_train = size(Y_train, 2);
n_test = size(P_test, 1);

n_lambdas = length(model.fit_svd.lambda);
ix_lambdas = 1:n_lambdas; %usually only one lambda

predictions.rec = zeros([n_probes, n_test, n_lambdas]);


for ix_lambda=ix_lambdas

    w = model.Va * pinv(model.Sa) * reshape(model.fit_svd.beta(:,ix_lambda),size(model.Va,2),size(model.Ub,2)) * pinv(model.Sb) * model.Ub';

    pred = D * (w * P_test'); 
    train_aff = Y_train' * pred;

    % reconstruct predictions
    aff_ord = zeros(n_train, n_test);
    %        test_aff_norm = test_aff ./ repmat(sqrt(sum(test_aff.^2)), size(test_aff,1), 1);
    train_aff_norm = train_aff ./ repmat(sqrt(sum(train_aff.^2)), size(train_aff,1), 1);
    for i=1:n_test
        [s six] = sort(train_aff(:,i)); %sort proteins by affinity
        aff_ord(six,i)=(1:length(s))' > length(s)-n_nn; % find top n_nn most similar proteins
    end;
    Y_nn_rec = Y_train * (train_aff_norm .* aff_ord);
    
    aff_rec = ar_reconstruction(Y_train, pred);
    aff_rec_dot = Y_train' * aff_rec;
    [m, ix] = max(aff_rec_dot);
    Y_rec_nn = Y_train(:, ix);
    
    predictions.w(:,:,ix_lambda) = w;
    predictions.rec(:,:,ix_lambda) = aff_rec;
    predictions.pred(:,:,ix_lambda) = pred;
    predictions.train_aff(:,:,ix_lambda) = train_aff;    
    predictions.rec_nn(:,:, ix_lambda) = Y_rec_nn;
    predictions.nn_rec(:,:,ix_lambda) = Y_nn_rec; %model reconstruction predictions
end



